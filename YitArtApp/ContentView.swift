//
//  ContentView.swift
//  YitArtApp
//
//  Created by 千云锋 on 2021/11/5.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            PostListView()
                .navigationTitle("title")
        }
        
    }

}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
