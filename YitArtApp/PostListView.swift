//
//  PostListView.swift
//  YitArtApp
//
//  Created by 千云锋 on 2021/11/5.
//

import SwiftUI

struct PostListView: View {
    var body: some View {
        Group {
            if #available(iOS 14.0, *) {
                ScrollView {
                    LazyVStack(content: {
                        ForEach(postList.list) { post in
                            ZStack {
                                
                                NavigationLink(destination: Text("detail")) {
                                    PostCell(post: post)
                                        .foregroundColor(.black)
                                        .multilineTextAlignment(.leading)
                                }
                            }
                            .listRowInsets(EdgeInsets())
                            .background(.white)
                            
                        }
                    })
                }
            } else {
                List() {
                    ForEach(postList.list) { post in
                        ZStack {
                            PostCell(post: post)
                            NavigationLink(destination: Text("detail")) {
                                EmptyView()
                            }.hidden()
                        }
                    }
                    
                }
                .listStyle(PlainListStyle())
                .modifier(ListRemoveSeparator())
            }
        }
        
    }
}

struct ListRemoveSeparator: ViewModifier {
    func body(content: Content) -> some View {
        content
            .onAppear(perform: {
                UITableView.appearance().tableFooterView = UIView()
                UITableView.appearance().separatorStyle = .none
            })
            .onDisappear(perform: {
                UITableView.appearance().tableFooterView = nil
                UITableView.appearance().separatorStyle = .singleLine
            })
    }
}

struct PostListView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            PostListView()
                .navigationTitle("title")
                .navigationBarHidden(true)
        }
        
        
    }
}
