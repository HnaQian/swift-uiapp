//
//  PostCell.swift
//  YitArtApp
//
//  Created by 千云锋 on 2021/11/5.
//

import SwiftUI

struct PostCell: View {
    let post: Post
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            HStack(spacing: 5) {
                post.avatarImage
                    .resizable()
                    .scaledToFill()
                    .frame(width: 50, height: 50)
                    .clipShape(Circle())
                    .overlay(
                        PostVIPBadge(vip: post.vip)
                            .offset(x: 16, y: 16)
                    )
                VStack(alignment: .leading, spacing: 5) {
                    if post.vip {
                        Text(post.name)
                            .font(.system(size: 16))
                            .foregroundColor(Color(red: 243/255, green: 99/255, blue: 4/255))
                            .lineLimit(1)
                    } else {
                        Text(post.name)
                            .font(.system(size: 16))
                            .foregroundColor(Color(red: 60/255, green: 60/255, blue: 60/255))
                            .lineLimit(1)
                    }
                   
                    Text(post.date)
                        .font(.system(size: 11))
                        .foregroundColor(.gray)
                }
                .padding(.leading, 10)
                Spacer()
                if !post.isFollowed {
                    
                    Button(action: {
                        print("click follow btn")
                    }) {
                        Text("关注")
                            .font(.system(size: 14))
                            .foregroundColor(.orange)
                            .frame(width: 50, height: 26, alignment: .center)
                            .overlay(
                                RoundedRectangle(cornerRadius: 13)
                                    .stroke(Color.orange)
                            )
                    }
                }
            }
            Text(post.text)
                .font(.system(size: 17))
            if !post.images.isEmpty {
                PostImageCell(images: post.images, width: UIScreen.main.bounds.width-30)
            }
            
            Divider()
            
            HStack(spacing: 5) {
                Spacer()
                PostCellToolbarButton(image: "message",
                                      text: post.commentCountText, color: .black, action: {
                    print("click comment")
                })
                Spacer()
                PostCellToolbarButton(image: "heart",
                                      text: post.likeCountText, color: .black, action: {
                    print("click headr")
                })
                Spacer()
            }
            
            Rectangle()
                .padding(.horizontal, -15)
                .frame(height: 8)
                .foregroundColor(Color(red: 238/255, green: 238/255, blue: 238/255))
        }
        .padding(.horizontal, 15)
        .padding(.top, 15)
    }
}












struct PostCell_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            PostCell(post: postList.list[0])
        }
    }
}
















