//
//  YitArtAppApp.swift
//  YitArtApp
//
//  Created by 千云锋 on 2021/11/5.
//

import SwiftUI

@main
struct YitArtAppApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
